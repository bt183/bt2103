package ra.bt2103;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Bt2103Application {

    public static void main(String[] args) {
        SpringApplication.run(Bt2103Application.class, args);
    }

}
